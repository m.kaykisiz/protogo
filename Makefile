fraud: fraud-processor fraud-supervisor

fraud-processor:
	protoc -I proto/fraud proto/fraud/processor.proto --go_out=plugins=grpc:.

fraud-supervisor:
	protoc -I proto/fraud proto/fraud/supervisor.proto \
	--go_out=Mprocessor.proto=gitlab.com/m.kaykisiz/protogo/fraud/processor,plugins=grpc:.
